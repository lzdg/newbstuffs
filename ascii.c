/* Text to ASCII converter I made years ago when I first started learning C.
 * Sometimes abuse is the most efficient way to get results.
 * 
 * It works by receiving a string and printing each character as an integer,
 * which obviously ends up printing the character's ASCII code.
 * From there I simply separated the codes with a space for coherence.
*/

#include <stdio.h>

int main()
{
	int x;
	
	while ((x = getchar()) != EOF) {
		printf("%d ", x);
	}
}
